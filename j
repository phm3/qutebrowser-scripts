#!/bin/bash 
# 
# Behaviour
#   Userscript for qutebrowser which pretty prints json formatted output
#
# Usage
#   You can launch the script from qutebrowser as follows:
#     :spawn --userscript j
#   Or add a key binding:
#    j = spawn --userscript /path/to/j
#
# Dependencies
# 
#  jq, pygmentize, json.tool
# 
#  The html template in not applicable on Mac os (at least in this version)
# 
# Akncowledge
#  This is a modification of script by Bryan Gilbert
#  https://github.com/qutebrowser/qutebrowser/blob/master/misc/userscripts/format_json 
# 
# Author
# github.com/phm3


STYLE=${1:-native}



if [[ `uname` == 'Linux' ]]; then
   FORMATTED_JSON="$(cat "$QUTE_TEXT" | jq '.' | pygmentize -l json -f html -O full,style=$STYLE)" 
   TEMP_FILE="$(mktemp --suffix '.html')" 

elif [[ `uname` == 'Darwin' ]]; then
   tempfoo=`basename $0`
   TEMP_FILE=`mktemp /tmp/${tempfoo}.XXXXXX` || exit 1
   #echo "$FORMATTED_JSON" > $TEMP_FILE
   FORMATTED_JSON="$(cat "$QUTE_TEXT" | python -m json.tool)"
fi


echo "$FORMATTED_JSON" > $TEMP_FILE

# send the command to qutebrowser to open the new file containing the formatted json
echo "open -t file://$TEMP_FILE" >> "$QUTE_FIFO"
